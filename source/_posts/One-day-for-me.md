---
title: One day for me!
date: 2013-06-28 23:17:21
tags: [Poem,writing]
---
I've written this right before my birthday!


_I wish for one day_,
a day with no ugly emotions,
away from plastic smiles,
and masters of exasperation;

I stand among baffling crowd,
annoying ditching and whispering loud,
where sincerity is at stake,
and even laggers are proud;

Confused I walk, many *_icons_*  to follow,
accompanied by wagers and cheats,
Who is correct?
the answer is too shallow;

Money, fame and name,
Are they the only aim?
the hunt begins everyday,
let me be away from them!

Nothing is permanent,
not even the pain,
loneliness bites,
and I'm helpless again;

I wish for one day ,
with some one who cares,
in the name of *'love'*
make me feel better again!
