---
title: Faking the acting
date: 2017-09-20 21:29:38
tags:
---
I have been lucky enough to witness stage plays ( one acts, mono acts, skits ) and street plays, engaging in various roles from audience,just helper, backstage guy, role with supporting character and recently happened to debut as a noob writer and noob director too. Been part of acting workshops!
Been lucky to perform at Mumbai University stage where legends have once performed, where the team I was a part of secured a rank too. Witnessed plays at INT although couldn't make to it in recent months!

What I feel fascinating about performing an act is you get a chance to divulge into a personality you could never think of but then you eventually make to it. __You be what you are not__ for that short span of time.

Here's a list of contributions that I could make!

# Handi
Skit "Handi" is a pack of ultimate comedy that touches sensitive issues in our society in altogether different way.

This skit was performed on 9th Sept 2015, on Teachers day celebration in S.P.I.T college, Andheri - organized by ACE Committee.

{% youtube AOU-jHAFp48 %}

## Making of handi
{% youtube FqTnYOKsCcg %}



# Marathi skit aga aga

Marathi Skit "Aga Aga" performed by students of MCA department, SPIT in the event "Talent Hunt" organized by ACE Committee ,MCA department.

Plot: 
Parents of Varsha (Vrushali &Sarvesh) do not accept the love of Varsha & Vishal. To make their parents agree, friends of Varsha and Varsha come up with a funny yet powerful plan. To know about the plan - watch the skit.

 {% youtube 4qgzq05WLM0 %}


(Sorry about the abrupt ending of video, there were technical issues. The end is simple as every successful love story ends, with parents of Varsha accepting their love! 

[ meta information : Vishal has offer letter from Morgan Stanley :P :P ]
)

