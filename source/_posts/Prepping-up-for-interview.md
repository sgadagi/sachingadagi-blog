---
title: Prepping up for campus interview
date: 2017-03-04 17:51:18
tags:
---

You are reading this probably because you've got companies lined up for campus recruitment process in next 3 months or four and you want to make sure you prep up very well _or_ probably you have no clue what to do. Even If you are in the second category, do not worry, so was I, but its the right time to make your moves.

#### But wait, why should I read _your_ post?

I am no expert in cracking the interviews but I have been a training and placement co-ordinator for about 6 months in my college and have seen the recruitment process very closely.

I'll begin by saying  _if you prepare very well for one company, you automatically prepare  for other companies as well._


## Initial Survey

I made sure I visited each and every company's website and tried to understand what is the nature of business they do, what kind of talent they look for and will I fit into the companys culture (answer to this is obtained only when you get answer for first two questions and ask yourself).
The answer to the third question would be blur initially but as you keep on doing the same, you'll definitely find point of inclination.


#### What kind of companies visit the college?
  - Investment Banks
  - Product based (MNCS, midsize, startups)
  - Service based (Majorly MNCs and midsize)
  - IT Consultancy (MNCs)
  - Data Analytics / Data driven companies

## The Checklist
I started reading interview experiences of all the candidates of all the companies online (glassdoor, quora, seniors) and tried to find common pattern in respective companies.
Then I started my interview preparation by compiling a checklist, and here it is

### DataStructures & Algorithms
Analysis of algorithms
Notations
Recursion
BackTracking
LinkedLists  Common - (Singly, Doubly, circular), Uncommon (Unrolled, XOR)
Stacks & Queues (Linear, circular, Priority)
Trees (Binary, NAry, Binary Search Trees, AVL , XOR)
Heaps
Graphs (Traversals, Shortest Path, Minimum Spanning Tree)
Sorting (Bubble, Shell, Selection, Quick, Merge, Heap)
Searching
Hashing
Greedy algorithms
Divide and Conquer problems
Dynamic Programming based questions
Ad-Hoc questions


### DataBase Management Systems
All commands under DDL, DML, DDL ,TCL
Normalization
JOINS ( Natural, Inner, Equi, Cross, Left Outer, Right outer)
Subqueries
Locks
Query Optimization


### Java

Core Java
Java Collections
Annotations

### Round Wise preparation
From the feedback I received, this section is needs rewamping.
The content will be beautified, segregated and back soon. :)
