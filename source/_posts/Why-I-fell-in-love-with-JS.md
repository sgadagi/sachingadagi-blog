---
title: Why I fell in love...with JS
date: 2017-10-01 20:43:14
tags: [Programming,Technology]
---

I've been programming since 2011 (2009 officially in educational records), learnt (and worked with) these languages- C, C++, Java, PHP, Python, C#, HTML, CSS and ofcourse the most intutitive, interesting and conventionally different I could find is *JavaScript* (wait let's not get into ES5 or ES6 or altogether any different flavor of  `web script` )

## The naive beginning
In 2011 there was a happy realization I had when I started to gain the real power of programming and that I have the power to change the way we look at the world by just developing a few HTML pages, little bit of server side scripting and a free web host & domain ( **000webhost** webserver, `.tk` and `.co.cc` domains were available to the rescue) so I decided to pull up my socks and started to develop a website for my dad's computer institute in 2012 wherein bootstrap was not even cool trend to follow on and just to have a dropdown on hover of horizontal `li`s on menu I had to generate a snippet of code using dreamweaver plugin for dropdown, that is when I met `JavaScript`.

## The urge to manipulate DOM and rise of jQuery

So I had a slider developed as a __flash__ and was a part of carousal and I found oh, this little piece of flash is what I don't find my piece of work. __Now what is to be done__? The answer was ___DOM manipulation___, how to do it? the answer was `jQuery`.


The JavaScript code that I used to write (_or copy_ perhaps,) was something I found very unusual and the curiosity then began creeping in.

JavaScript actually is interpreted but then where does the interpreter hide? For Java I know there's something called JVM that exists to compile it (I was a bad googler then,).

Sometimes I see `var` sometimes I don't? What's the real deal here?

Oh I see something horrendous at the very beginning of the jQuery code 

```
$(function(){
});

```

Then I read about the [IIFE's](https://en.wikipedia.org/wiki/Immediately-invoked_function_expression), and then again realization stuck that the above code **is NOT really IIFE** :P , it is just a shorthand expression of `$(document).ready(function(){
});`


### JS was, is and always will be lenient (and equally weird too!)

Most of programmers tend to spend hours debugging to find *missing semicolon* is what is said, but wait, JS doesn't expect much from you.

you don't declare a variable? `OK`
you don't append semicolon? `OK`
you compare `2` with `"2"` ? `OK` (definitely when using `==`, now don't bash me)
you wanna use `'` instead of `"`? `OK`
`"1"` + 0 is `10` but `1` - 0 is 0.

```
if(condition){
	var x = 'foo';
}
else {
	var y = 'bar';
}

console.log(x,y);

```

 No `ReferenceError` error? there something spicy called variable hoisting

### I come from functional language background, show me some of it!

Only to realize 

```
var foo = function(){
	
} 
```
and

```
function foo(){
	
}
```

[ARE NOT FREAKING SAME](https://stackoverflow.com/questions/336859/var-functionname-function-vs-function-functionname)!!!



### call me back, callback!

so JavaScript to me by now has super power to accept functions as a parameter, return functions as a parameter, declare a function inside a function and return that function... and list goes on zzZZ.

And.. [know who's calling whom even without passing any parameter?](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments)


## I have learnt OOP, show me what you have to show!!

Show me the class dude, uh no.. wait. [You better read this](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS)


### AJAX into action, mighty JSON dominates

So by now RESTful webservices are hot topic, even though it's nowhere mentioned that JSON should be default return type in webserivces, it's everyone's favorite, all thanks to Mr. Douglas Crockford. JSON now is not just `JavaScript`s object notation!!

Soon I was able to develop mobile app that communicated with database layer of application, all thanks to `JavaScript`


Here's what I witnessed the major technology shift, in **parallel timeline**

### Plenty of JS frameworks are `hot` now, Angular,React, Vue oh, what else is new?
I started getting awestruck when I realized a scripting web language could help take form of MV* libraries, (not to forget Ember, Knockout the old and mighty players).


### I'm not there just for frontend...!
... said JavaScript rubbing the lamp and see genie (io.js / node.js) come out of it.


### Even I wanna be platform independent
.... the world welcomed `Electron.js`

### Why remain on computers when it's the era of smartphones...
... said React Native (ionic & cordova chuckled in background)


## hello, world said Es6
You wanted taste of OOP? Here's what I have for you. [Class and inhertiance in JS](https://www.sitepoint.com/understanding-ecmascript-6-class-inheritance/)

ES6 got some goodies for me (arrow operator, spread operator are my favourites)

```

square = function square(x) {
  return x * x;
};
[1, 2, 3].map(square);
```

looked slim ;) as

```
square = (x) => x *x ;
[1,2,3].map(square)
```
You see that little touch of `functional` programming?



## JS Proxies ? do we even use them?
We may not. But if you've used Vue.js and worked on [data & methods](https://vuejs.org/v2/guide/instance.html#Data-and-Methods), its all proxies!!.


### JS will never break your promise
JS being single threaded, always suffered from `callback tower of doom` or `callback hell`, [promises](https://developers.google.com/web/fundamentals/primers/promises) has made our lives easier, just to realize `$http({}).then()..` that you've used somewhere has been returning promise.. :D

and then only to realize

```

new Promise((res,reject) => reject("I am rejecting promise"))
				.then((arg) => console.log(arg))
				.then((undefined,arg) => console.log(arg));

```

is same as 

```
new Promise((res,reject) => reject("I am rejecting promise"))
				.then((arg) => console.log(arg))
				.catch((arg) => console.log(arg));

````
because `catch((arg)...)` is sugar coated as `then((undefined,arg)...)`



The more I know about JS, the more I don't know about it, that's what makes me love JS!!
When I am not at work and I surf something about JS makes me realize, [I don't know JS, but do you?](https://github.com/getify/You-Dont-Know-JS)






