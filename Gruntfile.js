module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: [],
    jshint: {
      files: ['Gruntfile.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },
    watch: {
      files: ['<%= jshint.files %>', 'assets/less/style.less', 'assets/less/mobile.less', 'assets/less/variables.less'],
      tasks: ['jshint', 'clean', 'uglify', 'copy', 'less', 'cssmin']
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.title %> <%= grunt.template.today("yyyy-mm-dd hh:MM:ss") %> */\n'
      },
      build: {
        src: [        
          
        ],
        dest: 'assets/js/app.min.js'
      }
    },
    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'assets/css/app.min.css' : [
            
          ]
        }
      }
    },
    'ftp-deploy': {
      build: {
        auth: {
          host: 'saiashirwad.com',
          port: 21,
          authKey: '<%= stg.auth_key %>'
        },
        src: 'public',
        dest: '/'
      }
    }
  });

  // Load the plugin
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-ftp-deploy');
  grunt.loadNpmTasks('grunt-stage');

  // Default task(s).
  grunt.registerTask('default', []);

};